// Copyright (c) 2017 Stefán Ingi Valdimarsson
//   
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#define N_KJ {{N_KJ}}
#define N_KJS_TOT {{N_KJS_TOT}}
#define N_JOS_TOT {{N_JOS_TOT}}
#define N_KJS_ARRAY {{N_KJS_ARRAY}}
#define N_JOS_ARRAY {{N_JOS_ARRAY}}

#define N_S {{N_S}}
#define N_L_TOT {{N_L_TOT}}
#define N_LANDS_TOT {{N_LANDS_TOT}}
#define N_L_ARRAY {{N_L_ARRAY}}
#define N_L_S_ARRAY {{N_L_S_ARRAY}}
#define L_S_START {{L_S_START}}

#define MIN_FRAC_NUMERATOR {{MIN_FRAC_NUMERATOR}}
#define MIN_FRAC_DENOMINATOR {{MIN_FRAC_DENOMINATOR}}

#define N_SIMUL {{N_SIMUL}}

#define S_L_N_L_TOT {{S_L_N_L_TOT}}