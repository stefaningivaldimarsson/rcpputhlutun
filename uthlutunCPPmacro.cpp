// Copyright (c) 2017 Stefán Ingi Valdimarsson
//   
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

//[[Rcpp::plugins(cpp11)]]
#include <Rcpp.h>
#include <algorithm>
#include <map>
#include <array>
using namespace Rcpp;

typedef struct {
  int listi;
  long long atkv;
  int saeti;
} KJUT;

typedef struct {
  int samtok;
  long long atkv;
  int saeti;
  bool virk;
} LAUT;

typedef struct {
  int samtok;
  int listi;
  int kjordaemi;
  long long atkv;
  long long heildar_atkv_kj;
  int saeti;
  bool virk;
} UTHL;

bool kj_rod(KJUT const &a, KJUT const &b) {
  return a.atkv*(b.saeti+1) < b.atkv*(a.saeti+1);
};

bool la_rod(LAUT const &a, LAUT const &b) {
  if (a.virk && b.virk) {
    return a.atkv*(b.saeti+1) < b.atkv*(a.saeti+1);
  } else if (b.virk) {
    return true;
  } else {
    return false;
  }
};

bool hl_rod(UTHL const &a, UTHL const &b) {
  if (a.virk && b.virk) {
    return a.atkv*(b.saeti+1)*b.heildar_atkv_kj < b.atkv*(a.saeti+1)*a.heildar_atkv_kj;
  } else if (b.virk) {
    return true;
  } else {
    return false;
  }
};

#include "uthlutunCPPparameters_rendered.cpp"

int n_nidurst_row = N_SIMUL;
int n_nidurst_col = N_LANDS_TOT;

// [[Rcpp::export]]
void uthlutaCPP(IntegerMatrix gogn, IntegerMatrix nidurstodur, int i_h) {
  int n_kj = N_KJ;
  int n_kjs[N_KJ] = N_KJS_ARRAY;
  int n_jos[N_KJ] = N_JOS_ARRAY;
  int n_l[N_KJ] = N_L_ARRAY;
  int n_jos_t[N_KJ];
  int atkv_kj[N_KJ];
  
  int n_jos_tot = N_JOS_TOT;
  
  int n_s = N_S;
  int n_l_s[N_S] = N_L_S_ARRAY;
  int l_start_s[N_S] = L_S_START;
  bool tilgr_s[N_S];
  int l_i_s[N_S];
  int atkv_s[N_S];
  
  int n_l_tot = N_L_TOT;
  int s_l[N_L_TOT] = S_L_N_L_TOT;
  
  KJUT kj_ut_t[N_S];
  KJUT kj_ut[N_KJS_TOT];
  LAUT la_ut_t[N_S];
  LAUT la_ut_tt;
  LAUT la_ut[N_JOS_TOT];
  UTHL ut_hl_t[N_L_TOT];
  UTHL ut_hl_tt;
  UTHL ut_hl[N_JOS_TOT];
  
  int gogn_pos = 0;
  int kj_ut_pos = 0;
  int i_l, i_kj, j_l, i_s, j_s, s_curr;
  int n_s_tilgr = 0;
  int atkv_tot = 0;
  
  for (i_s = 0; i_s < n_s; i_s++) {
    atkv_s[i_s] = 0;
    l_i_s[i_s] = 0;
    la_ut_t[i_s].samtok = i_s;
    la_ut_t[i_s].atkv = 0;
    la_ut_t[i_s].saeti = 0;
  }
  for (i_l = 0; i_l < n_l_tot; i_l++) {
    atkv_tot += gogn(i_h, i_l);
    atkv_s[s_l[i_l]] += gogn(i_h, i_l);
  }
  for (i_s = 0; i_s < n_s; i_s++) {
    if (MIN_FRAC_DENOMINATOR*atkv_s[i_s] >= MIN_FRAC_NUMERATOR*atkv_tot) {
      n_s_tilgr++;
      tilgr_s[i_s] = true;
    } else {
      tilgr_s[i_s] = false;
    }
  }
  
  for (i_kj = 0; i_kj < n_kj; i_kj++) {
    atkv_kj[i_kj] = 0;
    n_jos_t[i_kj] = 0;
    
    for (i_l = 0; i_l < n_l[i_kj]; i_l++) {
      kj_ut_t[i_l].listi = gogn_pos+i_l;
      kj_ut_t[i_l].atkv  = gogn(i_h, gogn_pos+i_l);
      kj_ut_t[i_l].saeti = 0;
      atkv_kj[i_kj] += gogn(i_h, gogn_pos+i_l);
    }

    std::sort(kj_ut_t,kj_ut_t+n_l[i_kj],&kj_rod);
    
    
    for (i_l = 0; i_l < n_kjs[i_kj]; i_l++) {
      kj_ut[kj_ut_pos+i_l] =kj_ut_t[n_l[i_kj]-1];
      kj_ut[kj_ut_pos+i_l].saeti += 1;
      
      j_l = n_l[i_kj]-1;
      while ( (j_l>0) && (kj_rod(kj_ut[kj_ut_pos+i_l], kj_ut_t[j_l-1])) ) {
        kj_ut_t[j_l]=kj_ut_t[j_l-1];
        j_l--;
      }
      kj_ut_t[j_l]=kj_ut[kj_ut_pos+i_l];
    }
    
    for (i_l = 0; i_l < n_l[i_kj]; i_l++) {
      s_curr = s_l[kj_ut_t[i_l].listi];
      la_ut_t[s_curr].atkv += kj_ut_t[i_l].atkv;
      la_ut_t[s_curr].saeti += kj_ut_t[i_l].saeti;
      la_ut_t[s_curr].virk = tilgr_s[s_curr];
      i_s = l_start_s[s_curr] + l_i_s[s_curr];
      ut_hl_t[i_s].samtok = s_curr;
      ut_hl_t[i_s].kjordaemi = i_kj;
      ut_hl_t[i_s].atkv = kj_ut_t[i_l].atkv;
      ut_hl_t[i_s].listi = kj_ut_t[i_l].listi;
      ut_hl_t[i_s].saeti = kj_ut_t[i_l].saeti;
      ut_hl_t[i_s].heildar_atkv_kj = atkv_kj[i_kj];
      ut_hl_t[i_s].virk = tilgr_s[s_curr];
      l_i_s[s_curr]++;
    }
    
    gogn_pos += n_l[i_kj];
    kj_ut_pos += n_kjs[i_kj];
  }
  
  std::sort(la_ut_t,la_ut_t+n_s,&la_rod);

  for (i_s = 0; i_s < n_s; i_s++) {
    std::sort(ut_hl_t+l_start_s[i_s],ut_hl_t+l_start_s[i_s]+n_l_s[i_s],&hl_rod);
  }

  i_s = 0;
  while (i_s < n_jos_tot) {
    s_curr = la_ut_t[n_s-1].samtok;
    for (i_l = 0; i_l < n_l_s[s_curr]; i_l++) {
      i_kj = ut_hl_t[l_start_s[s_curr]+n_l_s[s_curr]-1].kjordaemi;
      ut_hl_tt = ut_hl_t[l_start_s[s_curr]+n_l_s[s_curr]-1];
      if (n_jos_t[i_kj] < n_jos[i_kj]) {
        break;
      } else {
        for (j_s = l_start_s[s_curr]+n_l_s[s_curr]-1; j_s > l_start_s[s_curr]; j_s--) {
          ut_hl_t[j_s] = ut_hl_t[j_s-1];
        }
        ut_hl_tt.virk = false;
        ut_hl_t[j_s] = ut_hl_tt;
      }
    }
    if (i_l == n_l_s[s_curr]) {
      la_ut_tt = la_ut_t[n_s-1];
      for (j_s = 0; j_s < n_s-1; j_s++) {
        la_ut_t[j_s+1] = la_ut_t[j_s];
      }
      la_ut_tt.virk = false;
      la_ut_t[0] = la_ut_tt;
      continue;
    }
    la_ut[i_s] = la_ut_t[n_s-1];
    la_ut[i_s].saeti++;
    
    j_s = n_s-1;
    while ( (j_s>0) && (la_rod(la_ut[i_s], la_ut_t[j_s-1])) ) {
      la_ut_t[j_s]=la_ut_t[j_s-1];
      j_s--;
    }
    la_ut_t[j_s]=la_ut[i_s];
    
    ut_hl[i_s] = ut_hl_tt;
    ut_hl[i_s].saeti++;
    
    j_s = l_start_s[s_curr]+n_l_s[s_curr]-1;
    while ( (j_s>l_start_s[s_curr]) && (hl_rod(ut_hl[i_s], ut_hl_t[j_s-1])) ) {
      ut_hl_t[j_s]=ut_hl_t[j_s-1];
      j_s--;
    }
    ut_hl_t[j_s]=ut_hl[i_s];
    
    n_jos_t[i_kj]++;
    i_s++;
  }
  
  for (i_l = 0; i_l < n_s; i_l++) {
    nidurstodur(i_h,i_l) = 0;
  }
  for (i_l = 0; i_l < n_l_tot; i_l++) {
    nidurstodur(i_h,N_S+ut_hl_t[i_l].listi) = ut_hl_t[i_l].saeti;
    nidurstodur(i_h,s_l[ut_hl_t[i_l].listi]) += ut_hl_t[i_l].saeti;
  }
}

// [[Rcpp::export]]
IntegerMatrix uthlutaFylkiCPP(IntegerMatrix gogn) {
  IntegerMatrix nidurstodur = IntegerMatrix(n_nidurst_row, n_nidurst_col);
  for (int i_h = 0; i_h < n_nidurst_row; i_h++) {
    uthlutaCPP(gogn, nidurstodur, i_h);
  }
  return nidurstodur;
}